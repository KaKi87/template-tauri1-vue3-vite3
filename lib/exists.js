import {
    fs,
    path
} from '@tauri-apps/api';

import getPathSeparator from './getPathSeparator.js';

let _pathSeparator;

export default async filePath => {
    if(!_pathSeparator)
        _pathSeparator = await getPathSeparator();
    return (await fs.readDir(await path.join(filePath, '..')))
        .some(({ name }) => name === filePath.split(_pathSeparator).slice(-1)[0]);
};