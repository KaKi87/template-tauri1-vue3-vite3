import fs from 'node:fs/promises';

import { build } from 'vite';
import { createHtmlPlugin } from 'vite-plugin-html';
import createVuePlugin from '@vitejs/plugin-vue';

(async () => {
    const {
        tauri: {
            windows: [{
                title
            }]
        }
    } = JSON.parse(await fs.readFile('./src-tauri/tauri.conf.json', 'utf8'));
    await build({
        root: './src-vue',
        base: './',
        build: {
            outDir: '../dist'
        },
        plugins: [
            createHtmlPlugin({
                inject: {
                    data: {
                        title,
                        devtoolsScriptTags: ''
                    }
                }
            }),
            createVuePlugin()
        ]
    });
})().catch(console.error);