# `template-tauri1-vue3-vite3`

Opinionated [Tauri](https://github.com/tauri-apps/tauri) 1.x + [Vue](https://github.com/vuejs/vue) 3.x + [Vite](https://github.com/vitejs/vite) 3.x template

## Specs

- Developer-friendly
    - Includes automatically opening [Vue DevTools](https://github.com/vuejs/devtools)
    - Includes [destyle.css](https://github.com/nicolas-cusan/destyle.css)
- Cross-platform
  - `start` & `build` scripts
  - builds
- Fully dark app
- Includes additional methods
  - `execute` wraps [`Command.execute`](https://tauri.app/v1/api/js/classes/shell.command/#execute) with the OS default interpreter, allowing to run any command
  - `exists` is like Node's [`fs.existsSync`](https://nodejs.org/api/fs.html#fsexistssyncpath), but returns a `Promise`
  - `getPathSeparator` is like Node's [`path.sep`](https://nodejs.org/docs/latest/api/path.html#pathsep), but as a function returning a `Promise`

## Installation

### Automated

`curl -s https://git.kaki87.net/KaKi87/template-tauri1-vue3-vite3/raw/branch/master/clone.sh | bash -s hello-world`

### Manual

Clone existing repo & create new repo :

```bash
git clone https://git.kaki87.net/KaKi87/template-tauri1-vue3-vite3.git hello-world
cd hello-world
rm -r .git
git init
git add .
git commit -m ":tada: Initial commit"
```

Install dependencies : `yarn install`

## Usage

Start development server using `yarn start`
(outputs temporary files in `dist` & `src-tauri/target/debug`)

Create production build using `yarn build`
(outputs production file in `src-tauri/target/release`)

Create signed builds using `yarn build-signed` after using `yarn keygen` once
(requires [`jq`](https://stedolan.github.io/jq/))

Keep Tauri dependencies up to date using `yarn upgrade-tauri`

Develop everything inside `src-tauri` & `src-vue`

---

![](https://i.goopics.net/r334ab.png)