import fs from 'node:fs/promises';

import openVueRemoteDevtools from 'vue-remote-devtools-api';
import { createServer } from 'vite';
import { createHtmlPlugin } from 'vite-plugin-html';
import createVuePlugin from '@vitejs/plugin-vue';

(async () => {
    const
        {
            port: devtoolsPort
        } = await openVueRemoteDevtools(),
        {
            build: {
                devPath
            },
            tauri: {
                windows: [{
                    title
                }]
            }
        } = JSON.parse(await fs.readFile('./src-tauri/tauri.conf.json', 'utf8')),
        { port } = new URL(devPath);
    await (await createServer({
        root: './src-vue',
        server: {
            port,
            strictPort: true
        },
        plugins: [
            createHtmlPlugin({
                inject: {
                    data: {
                        title,
                        devtoolsScriptTags: `<script>window.__VUE_DEVTOOLS_PORT__ = '${devtoolsPort}'</script><script src="http://localhost:${devtoolsPort}"></script>`
                    }
                }
            }),
            createVuePlugin()
        ]
    })).listen();
})().catch(console.error);